const express = require('express');
const hbs = require('hbs');
const bd = require ("./db/data");
const app = express();



//configuracion para motor de vistas hbs
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');
hbs.registerPartials(__dirname + "/views/partials");

//correccion de rutas
//correccion de rutas
app.get("/", (request, response) => {
    response.render("index");
});

app.get("/index.html", (request, response) => {
    const inicio = []
    inicio.push(bd.inicio[0])
    response.render("index",{
        inicio: inicio
    });
});


app.get("/paginas/word_cloud.html", (request, response) => {
    response.render("word_cloud");
});
app.get("/paginas/curso.html", (request, response) => {
    response.render("curso");
});


const matriculas = [...new Set(bd.media.map((item) => item.matricula))];

app.get("/integrante/:matricula", (request, response, next) => {
    const matricula = request.params.matricula;
    if (matriculas.includes(matricula)) {
        const integranteFilter = bd.integrantes.filter((integrante) => integrante.matricula === matricula);
        const mediaFilter = bd.media.filter((media) => media.matricula === matricula);
        response.render('integrante', {
            integrante: integranteFilter,
            tipoMedia: bd.tipoMedia,
            media: mediaFilter
        });
    } else {
        next();
    }
});

app.use((req, res) => {
    res.status(404).render('404_error');
});

app.listen(3000, () => {
    console.log("El servidor se está ejecutando en http://localhost:3000");
    console.log("http://localhost:3000/");
});

